//index.js
const express = require("express");
const app = express();
app.get("/", (req, res) => { //รอฟังการ GET มายัง endpoint "/"
   res.send("Hello World"); //ตอบกลับ คืน "Hello World"
});
app.listen(4500); //บอกให้ server รอที่ port 4500